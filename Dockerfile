FROM    python:3.8.10-slim-buster

ARG     RELEASE=rolling

RUN     apt-get update && \
        apt-get install less vim jq curl wget libcurl4 openssl liblzma5 screen git procps -y && \
        apt-get upgrade -y && \
        rm -rf /var/lib/apt/lists/*

RUN     pip install --upgrade pip

RUN     mkdir -p /opt/evaluate
WORKDIR /opt/evaluate

RUN     if [ "$RELEASE" = "rolling" ]; then \
                pip install git+https://gitlab.com/gitlab-org/professional-services-automation/tools/utilities/evaluate.git --no-cache-dir; \
        elif [ "$RELEASE" = "official" ]; then \
                pip install gitlab-evaluate --no-cache-dir; \
        fi