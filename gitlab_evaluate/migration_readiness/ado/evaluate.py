from urllib.parse import urljoin
import requests
import base64
import sys
import time


class AdoEvaluateClient():
    def __init__(self, host, token):
        self.host = host
        self.total_repositories = 0
        self.total_disabled_repositories = 0
        self.total_uninitialized_repositories = 0
        encoded_pat = base64.b64encode(f":{token}".encode()).decode()
        self.headers = {
            'Authorization': f'Basic {encoded_pat}'
        }
        self.params = {
            'api-version': '7.0-preview'
        }

    def generate_request_url(self, host, api, sub_api=None):
        base_url = host
        if not base_url.startswith("https://"):
            base_url = f"https://{base_url}"
        if sub_api:
            base_url_parts = base_url.split("://")
            base_url = f"{base_url_parts[0]}://{sub_api}.{base_url_parts[1]}"
        return urljoin(base_url + '/', api)

    def get_descriptor(self, project_id, params=None):
        url = self.generate_request_url(self.host, api=f"_apis/graph/descriptors/{project_id}", sub_api="vssps")
        try:
            response = requests.get(url, headers=self.headers, params=params)
            response.raise_for_status()
            return response.json()["value"]
        except Exception as e:
            print(f"Error fetching descriptors {url}: {e}", file=sys.stderr)

    def get_project_administrators_group(self, project_id, params=None):
        scopeDescriptor = self.get_descriptor(project_id)
        url = self.generate_request_url(self.host, api=f"_apis/graph/groups?scopeDescriptor={scopeDescriptor}", sub_api="vssps")
        try:
            response = requests.get(url, headers=self.headers, params=params)
            response.raise_for_status()
            project_admins = next((item for item in response.json()["value"] if item["displayName"] == "Project Administrators"), None)
            if project_admins:
                return project_admins["originId"]
            else:
                return None
        except Exception as e:
            print(f"Error fetching descriptors {url}: {e}", file=sys.stderr)

    def get_project_administrators(self, project_id, params=None):
        project_group_id = self.get_project_administrators_group(project_id)
        url = self.generate_request_url(self.host, api=f"_apis/GroupEntitlements/{project_group_id}/members", sub_api="vsaex")
        try:
            response = requests.get(url, headers=self.headers, params=params)
            response.raise_for_status()
            admins = []
            for member in response.json()["members"]:
                admins.append(f"{member['user']['displayName']} <{member['user']['mailAddress']}>")
            return admins
        except Exception as e:
            print(f"Error fetching descriptors {url}: {e}", file=sys.stderr)

    def get_work_items(self, project_id, project_name, params=None):
        params.update(self.params)
        url = self.generate_request_url(self.host, api=f"{project_id}/_apis/wit/wiql")
        query = {
            "query": f"Select [System.Id], [System.Title], [System.State] From WorkItems WHERE [System.TeamProject] = '{project_name}'"
        }
        return requests.post(url, headers=self.headers, params=params, json=query)

    def get_release_definitions(self, project_id, params=None):
        params.update(self.params)
        url = self.generate_request_url(self.host, api=f"{project_id}/_apis/release/definitions", sub_api="vsrm")
        return requests.get(url, headers=self.headers, params=params)

    def get_build_definitions(self, project_id, params=None):
        params.update(self.params)
        url = self.generate_request_url(self.host, api=f"{project_id}/_apis/build/definitions")
        return requests.get(url, headers=self.headers, params=params)

    def get_commits(self, project_id, repository_id, params=None):
        params.update(self.params)
        url = self.generate_request_url(self.host, api=f"{project_id}/_apis/git/repositories/{repository_id}/commits")
        return requests.get(url, headers=self.headers, params=params)

    def get_prs(self, project_id, repository_id, params=None):
        params.update(self.params)
        url = self.generate_request_url(self.host, api=f"{project_id}/_apis/git/repositories/{repository_id}/pullrequests")
        return requests.get(url, headers=self.headers, params=params)

    def get_branches(self, project_id, repository_id, params=None):
        params.update(self.params)
        url = self.generate_request_url(self.host, api=f"{project_id}/_apis/git/repositories/{repository_id}/refs")
        return requests.get(url, headers=self.headers, params=params)

    def get_repos(self, project_id, params=None):
        params.update(self.params)
        url = self.generate_request_url(self.host, api=f"{project_id}/_apis/git/repositories")
        return requests.get(url, headers=self.headers, params=params)

    def get_project(self, project_id, params=None):
        params.update(self.params)
        url = self.generate_request_url(self.host, api=f"_apis/project/{project_id}")
        return requests.get(url, headers=self.headers, params=params)

    def get_projects(self, params=None):
        params.update(self.params)
        url = self.generate_request_url(self.host, api='_apis/projects')
        return requests.get(url, headers=self.headers, params=params)

    def get_users(self, params=None):
        params.update(self.params)
        url = self.generate_request_url(self.host, api='_apis/graph/users', sub_api="vssps")
        return requests.get(url, headers=self.headers, params=params)

    def get_agent_pools(self, params=None):
        params.update(self.params)
        url = self.generate_request_url(self.host, api='_apis/distributedtask/pools')
        return requests.get(url, headers=self.headers, params=params)

    def retry_request(self, request_func, params, *args, max_retries=2, retry_delay=2):
        for retry_count in range(max_retries):
            response = request_func(*args, params=params)
            if response.status_code == 200:
                return response
            print(f"Received error {response.status_code}. Retrying in {retry_delay} seconds...")
            time.sleep(retry_delay)
        print(f"Max retries reached ({retry_count}). Unable to complete the request.")
        # raise Exception(f"Failed to complete the request after {max_retries} retries: {response.status_code}")

    def test_connection(self, params=None):
        params.update(self.params)
        url = self.generate_request_url(self.host, api='_apis/ConnectionData')
        return requests.get(url, headers=self.headers, params=params)

    def handle_getting_project_data(self, project):
        params = {}
        total_repos = 0
        total_build_definitions = 0
        total_release_definitions = 0
        total_work_items = 0
        print(f"Fetching project data from {project.get('name')}")
        project_id = project["id"]
        project_name = project["name"]
        print(f"Retriving project administrators in {project_name}...")
        project_admins = self.get_project_administrators(project_id, params)
        if project_admins and isinstance(project_admins, list):
            project_admins_str = ', '.join(project_admins)
        else:
            project_admins_str = str(project_admins) if project_admins is not None else "No administrators found"

        print(f"Retriving total repositories, yaml definitions, classic releases and work items in {project_name}...")

        get_repos_response = self.retry_request(self.get_repos, params, project_id)
        if get_repos_response:
            total_repos = len(get_repos_response.json().get("value", []))

        get_build_definitions_response = self.retry_request(self.get_build_definitions, params, project_id)
        if get_build_definitions_response:
            total_build_definitions = len(get_build_definitions_response.json().get("value", []))

        get_release_definitions_response = self.retry_request(self.get_release_definitions, params, project_id)
        if get_release_definitions_response:
            total_release_definitions = len(get_release_definitions_response.json().get("value", []))

        get_work_items_response = self.retry_request(self.get_work_items, params, project_id, project_name)
        if get_work_items_response:
            total_work_items = len(get_work_items_response.json().get("workItems", []))

        project_data = {
            'Project ID': project['id'],
            'URL': project['url'],
            'Name': project['name'],
            'Total Repositories': total_repos,
            'Total Build Definitions': total_build_definitions,
            'Total Release Definitions': total_release_definitions,
            'Total Work Items': total_work_items,
            'Administrators': project_admins_str
        }
        return project_data

    def handle_getting_repo_data(self, project):
        repos_data = []
        params = {
            "$top": "100"
        }
        project_id = project.get("id")
        print(f"Fetching repositories for project {project_id}...")

        while True:
            response = self.get_repos(project_id, params)
            repos = response.json()
            self.total_repositories += len(repos.get('value'))
            repos_data.extend(repos.get('value'))
            for repo in repos['value']:
                print(f"Processing repository {repo['name']} in project {project_id}...")
                if repo["isDisabled"]:
                    self.total_disabled_repositories += 1
                if repo["size"] == 0:
                    self.total_uninitialized_repositories += 1

            # Print progress
            print(f"Project {project_id}: Retrieved and processed {self.total_repositories} repositories so far...")

            # Check if there's a next page
            if not any(key.lower() == "x-ms-continuationtoken" for key in response.headers):
                break  # No more pages
            # There is page, so get the continuation token for the next page
            params["continuationToken"] = response.headers["X-MS-ContinuationToken"]

        return repos_data
